# Consider two files having some lines of statements. Write a Python program to swap content present at middle line of first file with the content of last line of the second file. (Note: First file contains odd numbers of lines of statement)

try:
    with open("week7\\w7q6_file1", "r+") as file1:
        f1_content = file1.readlines()


except Exception as e:
    print("Error reading file 1", e)


try:
    with open("week7\\w7q6_file2", "r+") as file2:
        f2_content = file2.readlines()


except Exception as e:
    print("Error reading file 2", e)

# checking condition for file 1 and finding the mid index

f1_mid_index = len(f1_content) // 2

# finding the last index of file 2
f2_last_index = len(f2_content) - 1

f1_content[f1_mid_index], f2_content[f2_last_index] = (
    f2_content[f2_last_index],
    f1_content[f1_mid_index],
)

try:
    with open("week7\\w7q6_file1", "w+") as file1:
        file1.writelines(f1_content)
except Exception as e:
    print("Error writing file 1", e)

try:
    with open("week7\\w7q6_file2", "w+") as file2:
        file2.writelines(f2_content)
except Exception as e:
    print("Error writing file 2", e)

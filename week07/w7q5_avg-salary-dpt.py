# Consider two files that contain information about Employees and Departments in the
# following parameters: Employee (Name, EId, Salary, DID), Department (DID,
# DName, DLocation). Write a Python program to find the average salary of each
# department

import pandas as pd

department = pd.read_csv("week7\\Dept.csv")
employee = pd.read_csv("week7\\Employee.csv")

# print(employee)
# print(department)

#merging based on D_Id
merge_table = pd.merge(department,employee, on ="D_Id")
# print(merge)
average_salry = merge_table.groupby('D_Name')['Salary'].mean().reset_index()
average_salry.columns=["Department","Average Salary"]
print(average_salry)
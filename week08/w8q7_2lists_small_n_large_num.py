# Write a Python program to create a list of user’s supplied distinct integers having
# even number of elements. The program further creates two lists of equal lengths
# based on the original list, where first list is having all elements less than elements of
# second list. Display both the lists.


def avg(num_list):
    # checking if the argument passed is list or not

    sum = 0
    for i in num_list:
        sum += i
    return sum / len(num_list)


def split_list(num_list, avrg):
    list1 = []
    list2 = []
    for i in num_list:
        if i < avrg:
            list1.append(i)

        else:
            list2.append(i)
    print(f"splitted list-> {list1}, {list2}")


# taking user input
print(
    "<---------This program will split list -------------->\n\n Start entering the number for the list, press 'q' to stop entering "
)
l = []
while True:

    temp = input()
    if temp.isdigit():
        l.append(int(temp))
    elif temp == "Q" or temp == "q":
        # print("Quitted")
        break
    else:
        print(temp, "is invalid")

#calling avg func for average
average = avg(l)

#calling split function to split the list 
split_list(l, average)

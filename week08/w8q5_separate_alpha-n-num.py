# Write a python program that reads a string which contains English alphabets and  numbers. The program should create two lists L1 and L2, where L1 includes all the numbers present in the string while L2 includes all the alphabets of the string. 

print("This program will separate characters and numbers from a string")
main_str=input("Enter a random string with characters and numbers mixed: ")

char_list=[]
num_list=[]
for char in main_str:
    if char.isalpha():
        char_list.append(char)
    
    if char.isdigit():
        num_list.append(char)

print(char_list,"\n",num_list)

# Write a program in python to find vowels having maximum number of instances in a given file.
# (Note: File contains variety of data such as English alphabets, numbers etc.).


def count_vowels(main_str):

    dic_count = {}
    for char in main_str:
        if char.lower() == "a":
            dic_count["a"] = dic_count.get("a", 0) + 1

        if char.lower() == "e":
            dic_count["e"] = dic_count.get("e", 0) + 1

        if char.lower() == "i":
            dic_count["i"] = dic_count.get("i", 0) + 1

        if char.lower() == "o":
            dic_count["o"] = dic_count.get("o", 0) + 1

        if char.lower() == "u":
            dic_count["u"] = dic_count.get("u", 0) + 1

    # print(dic_count)
    for key, value in dic_count.items():
        if value == max(dic_count.values()):
            print(f'"{key}" is maximum number of times: {value}')


try:
    with open("week8\\file", "r") as file:
        file_content = file.read()
        count_vowels(file_content)

except Exception as e:
    print(e)

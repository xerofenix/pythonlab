# Given a list of numbers. Write a program to turn every item of a list into its
# square
l = []
print(
    "This program take numbers from you and give you the square of the every number\nNow, start entering. When you are done entering press q"
)

#loop for taking user input
while True:
    temp = input()
    if temp.isdigit():
        l.append(int(temp))
    elif temp == "Q" or temp == "q":
        print("Quitted")
        break
    else:
        print(temp, "is invalid")

l_sqr = []

for i in l:
    l_sqr.append(i * i)

print("Square of the inputted number(s)", l_sqr)

# Given a two Python list. Write a program to iterate both lists simultaneously
# and display items from list 1 in original order and items from list 2 in reverse
# order.

l1 = []
l2 = []
print(
    "This program will display 2 list, first one in the original order and second one in the reverse order\n Start entering number for the first and second list separated by space. Press any letter to quit entering"
)
#this loop is for taking user input in the list
while True:
    try:
        temp1, temp2 = map(int, input().split())

        l1.append(int(temp1))
        l2.append(int(temp2))
    except:
        print("Quitted")
        break


for i in range(0, len(l1)):
    print(l1[i], "   ", l2[-i-1])

# Write a program to count the number of occurrences of item 50 from below
# tuple tp1:
# tp1= (50, 10, 60, 70, 50)

print("Program to finding the count of 50")
tp1 = (50, 10, 60, 70, 50)

item = 50
count = 0


for i in tp1:
    if item == i:
        count = count + 1

print("The number of occurence of", item, "is", count)

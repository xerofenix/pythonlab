# Write a function to return True if the first and last number of a given list is
# same. If numbers are different then return False.


def first_last(list):
    n = len(list)
    if list[0] == list[n - 1]:
        return True
    else:
        return False


# the above function can also be written as
def first_last(list):
    if list[0]==list[-1]:
        return True
    else:
        return False


l = []
print("This program will check whether the first and last number entered by you is same or not.\n Now, enter numbers, when you are done entering just press 'q'")
while True:
    try:
        temp = input()
        if temp.isdigit():
            l.append(int(temp))
        elif temp=="Q" or temp=="q":
            print("Quitting")
            break
        else:
            print("Invalid input")
    except Exception as e:
        print(e)



ans = first_last(l)
if ans:
    print("Yes! The first and last number is same")
else:
    print("No! First and last number is not same")
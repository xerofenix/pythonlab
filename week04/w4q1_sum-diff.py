# Write a program to create function cal_sum_sub() such that it can accept two
# variables and calculate addition and subtraction. Also, it must return both
# addition and subtraction in a single return call.


def cal_sum_sub(a, b):
    sum = a + b
    diff = a - b
    return sum, diff


var1 = float(input("Enter a number to add and subtract: "))
var2 = float(input("Enter another number for the same: "))

ans = []
ans.append(cal_sum_sub(var1, var2))

#destructuring
res1,res2=cal_sum_sub(var1,var2)
print("sum is ",res1,"\ndifference is",res2)
print("sum and difference", ans)

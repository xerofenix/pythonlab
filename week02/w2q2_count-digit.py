# Write a program to count the total number of digits in a number using a while loop

num = int(input("Enter a number to count the digit(s): "))
count = 0
while num:
    count += 1
    num //= 10

print("Total number of digit(s) are ", count)
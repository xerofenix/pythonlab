# Write a program to display all prime numbers within a range


def is_prime(n):
    if n <= 1:
        return False
    for i in range(2, int(n**0.5) + 1):
        if n % i == 0:
            return False
    return True


def print_primes(start, end):
    for num in range(start, end + 1):
        if is_prime(num):
            print(num)


start = int(input("Enter the start of the range: "))
end = int(input("Enter the end of the range: "))

print("Prime numbers within the specified range are ")

print_primes(start, end)
# Write a program to use the loop to find the factorial of a given number

n = int(input("Enter a number to find the factorial: "))

fact = 1
for i in range(1, n + 1):
    fact = fact * i

print("Factorial of", n, "is", fact)

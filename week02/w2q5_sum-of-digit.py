# Write a program to find the sum of digits of a supplied integer

num = int(input("Enter a number: "))
digit = 0
sum = 0
while num:
    digit = num % 10
    sum = sum + digit
    num //= 10

print("The sum of all digit(s) is", sum)

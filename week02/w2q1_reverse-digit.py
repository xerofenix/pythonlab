# Write a Program to extract each digit from an integer in the reverse order.

num = int(input("Enter a number: "))
digit = []

while num:
    digit.append(num % 10)
    num //= 10

print("reversed digits are ", digit)
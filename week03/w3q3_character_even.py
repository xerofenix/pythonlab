# Write a program to print characters from a string which are present at an even
# index numbers

str = "This is testing string to print even index characters"

print("Printing characters at even index")
print(str[1::2])

# Write a program to remove characters from a string starting from n to last and
# return a new string. Example: remove_chars("aligarh", 3) so output must be
# al.


def remove_chars(string, start):
    result = string[: start - 1]
    return result

str = input("Enter a string: ")
n = int(input("Enter a number (position) from which you want the characters to be removed upto last: "))

ans = remove_chars(str, n)
print(ans)

## 5. Write a program in python to find word/s having maximum number of instances in a given file and replace all its occurrences with “Aligarh”


def count_word(content):
    """this function will receive a list of words and count the maximum occuring word"""
    count_words = {}
    for word in content:
        count_words[word] = count_words.get(word, 0) + 1
    print(count_words)

    max_occ_word = ""
    no_occ = 0

    for key, value in count_words.items():
        if value == max(count_words.values()):
            print(f'"{key}" is occuring maximum number of times, i.e {value}')
            max_occ_word = key
            no_occ = value
    
    return max_occ_word, no_occ


try:
    with open("week10\\w10q1.txt", "r") as file:
        file_content = file.read().split()
    # print(file_content)
except Exception as e:
    print("Error reading file", e)

word_max_times, max_times = count_word(file_content)

for i in range(len(file_content)):
    if file_content[i]==word_max_times:
        file_content[i]="Aligarh"


count_word(file_content)

## 6. Consider two files that contain information about Employees and Departments in the following parameters: Employee (Name, EId, Salary, DID), Department (DID, DName, DLocation). Write a Python program to merge the content of both the file in following format.: Emp_Dep(Ename, Eid, Esalary, EDID, DName,Dlocation) (Note: Merging should follow the condition-DID of Employee file should be equal to Department ID of department file)

import pandas as pd

dept = pd.read_csv("week10\\Dept.csv")
emp = pd.read_csv("week10\\Employee.csv")

print("---------Department info------------------\n", dept)
print("\n---------Employee info----------\n", emp)

#mergin table
merge_table = pd.merge(dept, emp, on="D_Id")

#making data frame 
# df = pd.DataFrame(merge_table)

#rearranging the column
new_order = ["Name", "E_Id", "Salary","D_Id","D_Name","D_Location"]
df = merge_table[new_order]

#renaming the column
df.columns = ["EName","Eid","Esalary","EDID","DName","Dlocation"]
print("\n------------------------Merged table------------------------------\n",df)

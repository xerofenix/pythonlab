# 7. A file contains information about employees with the following parameters: Name, 
# Id, Salary, Dname. Write a Python program to write one more column HRA (House 
# rent allowances) to this file, where HRA= 18%of salary 
# Eg: Suppose the existing file is as follows, where you need to add HRA column: 
# Name,id,salary, Dname 
# Amar,101,20000,Sales 
# Ammar,102,22000,Marketing 
# Rahil,103,18000,Sales

import pandas as pd
try:
    file_data = pd.read_csv("week9\\w9q7.csv")
except Exception as e:
    print(e)
print(file_data,"\n")
file_data["HRA"]=file_data["salary"]*(18/100)
print(file_data)
## 6. A file contains information about programs and courses in the following format: Program,course. Write a Python program to find the number of courses against each program. 
#Eg:  
#Program,Course   
#MCA,Database   
#MCA,Java   
#M.Sc,Data Structure   
#B.Sc, Python   
#The output should be: MCA-2, M.Sc-01, B.Sc-01

import pandas as pd

try:
    data = pd.read_csv("week9\\file6")
    print(data)
except Exception as e:
    print(e)
grouped = data.groupby("Program")['Course'].count()
print(grouped)
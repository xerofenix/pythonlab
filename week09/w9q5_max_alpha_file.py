## 5. Write a program in python to find alphabet/s having maximum number of instances in a given file.

try:
    with open("week9\\w9q5_file", "r+") as file:
        file_content=file.read()
        char_dict={}
        for i in file_content:
            if i is not " ":
                char_dict[i]=char_dict.get(i,0)+1
        for key,value in char_dict.items():
            if value == max(char_dict.values()):
                print(f'"{key}" is occuring maximum number of times, i.e {value}')

except Exception as e:
    print("Error reading file",e)
# Write a program create a numpy array and sort as per below cases:
# a. Case 1: Sort array by the second row.
# b. Case 2: Sort the array by the second column.

import numpy as np

print(
    "<--------This program will create a numpy array and sort on the basis of second row and second column---->"
)
# taking size of array
try:
    m, n = map(
        int, input("Enter size of matrix (2D Array) separated by space: ").split()
    )
except:
    print("Quitted")


l = []

# taking element input and storing in list
print("Start entering the numbers")
for i in range(m * n):

    temp = input()
    if temp.isdigit():
        l.append(int(temp))
    else:
        print(temp, "is invalid")

# making 2D numpy array of given shape
np_array = np.array([l])
np_array.shape = m, n


# Case 1: Sort array by the second row
# Sort columns based on the values in the second row
sorted_by_second_row = np_array[:, np_array[1, :].argsort()]

# Case 2: Sort the array by the second column
# Sort rows based on the values in the second column
sorted_by_second_column = np_array[np_array[:, 1].argsort()]

print("Original Array:")
print(np_array)

print("\nArray sorted by the second row:")
print(sorted_by_second_row)

print("\nArray sorted by the second column:")
print(sorted_by_second_column)

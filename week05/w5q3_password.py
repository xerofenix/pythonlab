# Write a program to generate a random Password which meets the following conditions
# a. Password length must be 10 characters long.
# b. It must contain at least 2 upper case letters, 1 digit, and 1 special symbol.

import random


def password_generator():

    upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    lower_case = "abcdefijklmnopqrstuvwxyz"
    special_char = "!@#$%^&*~?<>"
    digit = "0123456789"

    password = []
    for i in range(10):
        if i < 2:
            password.append(upper_case[random.randint(0, len(upper_case) - 1)])

        if i > 1 and i < 8:
            password.append(lower_case[random.randint(0, len(lower_case) - 1)])

        if i > 7 and i < 9:
            password.append(special_char[random.randint(0, len(special_char) - 1)])

        if i > 8 and i < 10:
            password.append(digit[random.randint(0, len(digit) - 1)])

    # this will shuffle the password
    random.shuffle(password)

    # join the list of characters with join function, putting anything in the inverted commas will be inserted in each joint. see example by hovering on join
    passw = "".join(password)
    return passw

print("This program will generate password")
while True:
    user_inp = input("Press enter to generate, q to quit: ")
    if user_inp == "":
        password = password_generator()
        print(password)
    elif user_inp == "q" or user_inp == "Q":
        print("Quitted")
        break
    else:
        print("Invalid input")

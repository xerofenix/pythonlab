# Write a program to create a numpy array and return array of odd rows and even
# columns from the numpy array.

import numpy as np

print(
    "<--------This program will create a numpy array and give array of odd rows and even column---->"
)
# taking size of array
try:
    m, n = map(
        int, input("Enter size of matrix (2D Array) separated by space: ").split()
    )
except:
    print("Quitted")


l = []

# taking element input and storing in list
print("Start entering the numbers")
for i in range(m * n):

    temp = input()
    if temp.isdigit():
        l.append(int(temp))
    else:
        print(temp, "is invalid")

# making 2D numpy array of given shape
np_array = np.array([l])
np_array.shape = m, n
print(np_array)

arr = []

# appending elements of odd rows and even columns
for i in range(0, len(np_array), 2):
    for j in range(1, len(np_array[0]), 2):
        arr.append(np_array[i][j])


print(arr)

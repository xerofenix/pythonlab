# Given a two list of numbers, write a program to create a new list such that the
# new list should contain odd numbers from the first list and even numbers from
# the second list.

l1 = []
l2 = []

merge_list = []
print(
    "<---------This program will merge odd numbers from the first and even numbers from the second list-------------->\n\n Start entering the number for the first list, press 'q' to stop entering "
)
while True:

    temp = input()
    if temp.isdigit():
        l1.append(int(temp))
    elif temp == "Q" or temp == "q":
        print("Quitted")
        break
    else:
        print(temp, "is invalid")
print("Start entering for the second list, press 'q' to quit entering")
while True:

    temp = input()
    if temp.isdigit():
        l2.append(int(temp))
    elif temp == "Q" or temp == "q":
        print("Quitted")
        break
    else:
        print(temp, "is invalid")

for i in l1:
    if i % 2 != 0:
        merge_list.append(i)

for i in l2:

    if i % 2 == 0:
        merge_list.append(i)


print(merge_list)

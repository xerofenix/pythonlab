# Write a program to pick a random character from a given String supplied by
# the user.
import random

sentence = input("Enter a string / sentence to pick a random character: ")

#this loop is for printing the output based on user's choice
while True:
    temp = input("Press enter to show, q to quit: ")
    if temp == "q" or temp == "Q":
        print("quitted")
        break
    elif temp == "":
        random_no = random.randint(0, len(sentence) - 1)
        # print(random_no)
        character = sentence[random_no]
        print("random picked character: ", character)
    else:
        print("Invalid input")

# Write a program that inputs a main string and creates an encrypted string by 
# embedding a short symbol-based string after each character. The program should 
# also decrypt the string. 

def encrypt(main_string, symbol):
    """Encrypts the main string by embedding the symbol after each character."""

    encrypted_string = "".join(char + symbol for char in main_string)
    return encrypted_string


def decrypt(encrypted_string, symbol):
    """Decrypts the encrypted string by removing the symbol after each character."""

    decrypted_string = encrypted_string.replace(symbol, "")
    return decrypted_string


# Main program
if __name__ == "__main__":
    main_string = input("Enter the main string to encrypt: ")
    symbol = input("Enter the symbol to embed: ")

    # Encrypt the string
    encrypted_string = encrypt(main_string, symbol)
    print("Encrypted string:", encrypted_string)

    # Decrypt the string
    decrypted_string = decrypt(encrypted_string, symbol)
    print("Decrypted string:", decrypted_string)

# below can be used to print the doc (string/info within """ this""")
# print(encrypt.__doc__)

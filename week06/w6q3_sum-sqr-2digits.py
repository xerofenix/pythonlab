# Write a Python program having a void function that receives a 4-digit number
# and calculates the sum of squares of first 2 digits’ number and last two digits’
# number, e.g. if 1233 is passed as argument then function should calculate 12^2 + 33^2.


# function performing sum of sqaure first and last 2 digit number
def sum_sqr_fist_and_last_2_digits(num):

    sum = 0
    while num:
        two_digits = num % 100
        sum += pow(two_digits, 2)
        num //= 100

    print("The sum of the square of first and last 2 digit is", sum)


print(
    "This program will perform sum of square of first and last 2 digits of a 4 digit number"
)

# taking user input
while True:
    try:
        user_inp = int(input("Enter a 4 digit number: "))
        if user_inp < 1000 or user_inp > 9999:
            print("not a 4 digit number")
            continue
        break
    except:
        print("Invalid input")

sum_sqr_fist_and_last_2_digits(user_inp)

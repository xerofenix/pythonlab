# Write a program to accept a string and display the following:
# a. Number of uppercase characters
# b. Numbers of lowercase characters
# c. Total number of alphabets
# d. Number of digits

string = input("Enter a string to display uppercase, lowercase, total alphabet and digits: ")
uppercase = 0
lowercase = 0
alphabets = 0
digits = 0

for i in string:
    if i.isupper():
        uppercase += 1
    elif i.islower():
        lowercase += 1
    elif i.isdigit():
        digits += 1


print("number of uppercase letters: ",uppercase,)
print("number of lowercase letters:",lowercase,)
print("number of digits:",digits,)

# Write a program to get roll numbers, names, and marks of students and store these 
# details in a file called "Marks. data". 

def save_to_file(filename, student_data):
    """Saves student data to a file."""
    with open(filename, 'a') as file:
        file.write(f"{student_data[0]}, {student_data[1]}, {student_data[2]}\n")

def main():
    filename = "Marks.data"
    print("Enter student details (type 'exit' as roll number to stop):")
    
    while True:
        roll_number = input("Enter roll number: ")
        if roll_number.lower() == 'exit':
            break
        name = input("Enter name: ")
        marks = input("Enter marks: ")
        save_to_file(filename, (roll_number, name, marks))
        print("Student details saved.")

if __name__ == "__main__":
    main()

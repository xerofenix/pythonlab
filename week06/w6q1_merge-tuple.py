# Write a Python program that inputs two tuples and creates a third that contains
# all elements of the first followed by all elements of the second. (You may use
# other data types such as lists etc. to make your program work)


print("This program will create tuple and concatinate them")

l1 = []
l2 = []

print("Start Entering the elements for tuple 1, when you are done press 'q'")
while True:
    temp = input()
    if temp.isdigit():
        l1.append(int(temp))
    elif temp == "Q" or temp == "q":
        print("Quitted")
        break
    else:
        print(temp, "is invalid")

print("Now enter element for second tuple, when you are done press 'q'")
while True:
    temp = input()
    if temp.isdigit():
        l2.append(int(temp))
    elif temp == "Q" or temp == "q":
        print("Quitted")
        break
    else:
        print(temp, "is invalid")

# i can also directly print the tuple(l1+l2) but i have to create 3rd tuple as per the question
tup3 = tuple(l1 + l2)
print(tup3)
